"""
Import gpodder data
"""

import sqlite3
import pyotherside
import os

def get():
    home = os.path.expanduser('~')
    xdg_data_home = os.environ.get('XDG_DATA_HOME', os.path.join(home, '.local', 'share'))
    data_home = os.path.join(xdg_data_home, 'harbour-org.gpodder.sailfish')
    database = os.path.join(data_home, 'Database.minidb')
    conn = sqlite3.connect(database)

    c = conn.cursor()
    d = conn.cursor()

    channels = c.execute('SELECT * FROM PodcastChannel')
    for channel in channels:
        cid = channel[0]
        url = channel[2]
        pyotherside.send("import channel %s" % url)
        if url:
            yield url

        # posts = d.execute('SELECT * FROM PodcastEpisode WHERE podcast_id=%d' % cid)
        # print(cid, url)
        # for post in posts:
        #     print(post[2])
        # print()

