"""
The inbox queue. This also uses a Factory for instancing
"""

import sys
import sys
import os
import time

sys.path.append("../")

from podcast.singleton import Singleton
from podcast.factory import Factory
from podcast.store import Store
from podcast.archive import ArchiveFactory
from podcast.queue import QueueFactory

from podcast import util
import pyotherside

inboxname = 'the_inbox'

class Inbox:
    """
    The Inbox. It has a list of podposts
    """

    def __init__(self):
        """
        Initialization
        """
                
        self.podposts = []              # Id list of podposts

    def save(self):
        """
        pickle this element
        """

        store = Factory().get_store()
        store.store(inboxname, self)

    def insert(self, podpost):
        """
        Insert an podost
        """

        if podpost not in self.podposts:
            self.podposts.insert(0, podpost)
            post = Factory().get_podpost(podpost)
            if post.insert_date == None:
                post.insert_date = time.time()
            post.save()
            pyotherside.send("Inbox element saved %d" % len(self.podposts))
        self.save()

    def get_podposts(self):
        """
        get the list of podposts
        """

        posts = []
        for podpost in self.podposts:
            yield podpost

    def remove(self, podpost):
        """
        Remove a podpost from archive
        """

        self.podposts.remove(podpost)

    def move_queue_top(self, podpost):
        """
        Move this podpost to queue_top (play)
        """

        QueueFactory().get_queue().insert_top(podpost)
        self.remove(podpost)
        self.save()
        
    def move_queue_next(self, podpost):
        """
        Move podpost to next queue position
        """

        QueueFactory().get_queue().insert_next(podpost)
        self.remove(podpost)
        self.save()
        
    def move_queue_bottom(self, podpost):
        """
        Move Podpost to the bottom of Queue
        """

        QueueFactory().get_queue().insert_bottom(podpost)
        self.remove(podpost)
        self.save()
        
    def move_archive(self, podpost):
        """
        Move Podpost to Archive
        """
        ArchiveFactory().get_archive().insert(podpost)
        self.remove(podpost)
        self.save()

    def move_all_archive(self):
        """
        Move all posts to Archive
        """

        archive = ArchiveFactory().get_archive()
        
        for post in self.podposts:
            archive.insert(post)
        self.podposts = []
        self.save()

class InboxFactory(metaclass=Singleton):
    """
    Factory which crates an Inbox if it does not exist or gets the pickle
    otherwise it returns the single element
    """

    def __init__(self, progname=('podcast')):
        """
        Initialization
        """

        self.inbox = None

    def get_inbox(self):
        """
        Get the Inbox
        """

        if not self.inbox:
            store = Factory().get_store()
            self.inbox = Factory().get_store().get(inboxname)
            if not self.inbox:
                self.inbox = Inbox()
                self.have_inbox = True
                return self.inbox

        return self.inbox
    
