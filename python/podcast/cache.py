"""
A memory cache with limited elements
"""
from collections import OrderedDict
import hashlib

class Cache:
    def __init__(self, limit=10):
        """
        Initialize the class. 
        limit: limit of elements to be stored
        """

        self.limit=limit
        self.elements = OrderedDict()

    def _hashme(self, name):
        """
        create a hash from name
        """
        
        return hashlib.sha256(name.encode()).hexdigest() + ".pickle"

    def _clean(self):
        """
        clean cache if length > limit
        """

        while len(self.elements) > self.limit:
            self.elements.popitem(last=False)

    def store(self, index, element):
        """
        store the element in self.elements
        index: Element index
        element: the element
        """

        fname = self._hashme(index)
        self.elements[fname] = element
        self._clean()

    def delete(self, index):
        """
        delete an element
        index: Element index
        """

        fname = self._hashme(index)
        if fname in self.elements:
            del self.elements[fname]

    def get(self, index):
        """
        get an element from cache
        index: Element index
        """

        fname = self._hashme(index)
        if fname in self.elements:
            return self.elements[fname]
        return None
    
    def exists(self, index):
        """
        does element exist?
        index: Element index
        """

        fname = self._hashme(index)
        return fname in self.elements

    @property
    def size(self):
        """
        return cache size
        """

        return len(self.elements)
