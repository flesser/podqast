<p>
<h3>Autoren</h3>
  <a href="https://talk.maemo.org/member.php?u=33544">Cy8aer</a> (coden)<br>
  <a href="https://talk.maemo.org/member.php?u=31168">BluesLee</a> (testen)<br>
  <a href="http://www.einbilder.de">Daniel Noll</a> (malen)
<h4>Übersetzungen</h4>
  <a href="https://gitlab.com/eson">Åke Engelbrektson</a> (sv)
  <a href="https://gitlab.com/carmenfdezb">Carmen F. B.</a> (es)
</p>

<p>
<h3><a href="https://gitlab.com/cy8aer/podqast">PodQast</a> verwendet</h3>
<a href="https://github.com/kurtmckee/feedparser">feedparser</a>
von Kurt McKee und Mark Pilgrim<br>
<a href="http://gpodder.org/mygpoclient/">mygpoclient</a> von Thomas Perl und anderen<br>
<a href="https://github.com/Alir3z4/html2text/">html2text</a> von Aaron Schwartz
und anderen
</p>

<p>
<h3>Datenschutz</h3>
PodQast speichert alle Informationen auf dem Gerät. Für das Finden
von Podcasts verwendet PodQast den <a
href="https://gpodder.net">gpodder.net</a>-Dienst. Jeder angemeldete
Podcast-Anbieter kann Anwenderdaten anders verwenden. Bitte informiere
Dich darüber bei dem jeweiligen Anbieter.
</p>

<p>Copyright (c) 2018 Thomas Renard</p>
