import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.Portrait

    DockedPanel {
        id: parampanel
        width: parent.width
        height: parent.width
        dock: Dock.Top
        open: podqast.playparamdok
        z: 3

        Rectangle {
            anchors.fill: parent
            color: Theme.highlightDimmerColor
            opacity: 0.8
        }

        Slider {
            id: playrateSlider
            width: parent.width
            label: qsTr("Audio playrate")
            minimumValue: 0.85
            maximumValue: 2.0
            handleVisible: true
            valueText: "1:" + value
            stepSize: 0.15
            value: podqast.playrate
            onValueChanged: podqast.playrate = value
        }
        IconButton {
            icon.source: "image://theme/icon-m-up"
            onClicked: podqast.playparamdok = false
            anchors.bottom: parent.bottom
            anchors.right: parent.right
        }
    }

    SilicaFlickable {
        id: playflick
        anchors.fill: parent

        Row {
            id: imagecol
            width: page.width
            height: page.width

            Image {
                id: podimage
                width: parent.width
                height: parent.width
                source: playicon == "" ? "../../images/podcast.png" : playicon
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        podqast.playparamdok = true
                    }
                }
                IconButton {
                    icon.source: "image://theme/icon-m-down"
                    visible: podqast.playparamdok === false
                    onClicked: podqast.playparamdok = true
                    anchors.top: parent.top
                    anchors.right: parent.right
                }
            }
        }

        Column {
            id: playcontrols
            anchors.bottom: parent.bottom
            width: page.width
            Row {
                id: chapterrow
                width: parent.width
                height: Theme.itemSizeExtraLarge
                IconButton {
                    id: lastchapter
                    enabled: podqast.chapters.length > 0
                    anchors.left: parent.left
                    icon.source: "image://theme/icon-m-previous"
                    onClicked: {
                        if (podqast.chapters.length > 0) {
                            if (podqast.aktchapter !== 0) {
                                podqast.aktchapter -= 1
                                podqast.playpos = podqast.tomillisecs(podqast.chapters[podqast.aktchapter].start)
                                playerHandler.seek(podqast.playpos)
                                mediaplayer.seek(podqast.playpos)
                                chapterLabel.text = Number(podqast.aktchapter + 1) + ". " + podqast.chapters[podqast.aktchapter].title
                            }
                        }
                    }
                }
                Column {
                    id: infocolumn
                    anchors.left: lastchapter.right
                    anchors.right: nextchapter.left
                    height: parent.height
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if (podqast.chapters.length > 0) {
                                pageStack.push(Qt.resolvedUrl("Chapters.qml"))
                            }
                        }
                    }
                    Label {
                        id: titlelabel
                        width: parent.width
                        text: podqast.firsttitle
                        font.pixelSize: Theme.fontSizeSmall
                        font.bold: true
                        wrapMode: Text.WordWrap
                    }

                    Label {
                        id: chapterLabel
                        enabled: podqast.chapters.length > 0
                        anchors.top: titlelabel.bottom
                        width: parent.width
                        text: podqast.chapters.length + qsTr(" chapters")
                        font.pixelSize: Theme.fontSizeExtraSmall
                        wrapMode: Text.WordWrap
                        Timer {
                            id: secs
                            interval: 1 * 1000
                            running: podqast.playing
                            repeat: true
                            onTriggered: {
                                if (podqast.chapters.length !== 0) {
                                    podqast.getaktchapter()
                                    chapterLabel.text = Number(podqast.aktchapter + 1) + ". " + podqast.chapters[podqast.aktchapter].title
                                }
                            }
                        }
                    }
                }
                IconButton {
                    id: nextchapter
                    enabled: podqast.chapters.length > 0
                    anchors.right: parent.right
                    icon.source: "image://theme/icon-m-next"
                    onClicked: {
                        if (podqast.chapters.length > 0) {
                            if (podqast.aktchapter !== podqast.chapters.length -1 ) {
                                podqast.aktchapter += 1
                                podqast.playpos = podqast.tomillisecs(podqast.chapters[podqast.aktchapter].start)
                                playerHandler.seek(podqast.playpos)
                                mediaplayer.seek(podqast.playpos)
                                chapterLabel.text = Number(podqast.aktchapter + 1) + ". " + podqast.chapters[podqast.aktchapter].title
                            }
                        }
                    }
                }
            }

            Row {
                id: progressrow
                width: parent.width
                height: Theme.itemSizeLarge
                Slider {
                    id: playSlider
                    width: parent.width
                    leftMargin: 50
                    rightMargin: 50
                    height: parent.height
                    handleVisible: false
                    maximumValue: mediaplayer.duration
                    value: mediaplayer.position
                    valueText: to_pos_str(mediaplayer.position / 1000)
                    onPressedChanged: {
                        mediaplayer.seek(playSlider.value)
                    }
                }
            }
            Row {
                id: playerrow
                anchors.horizontalCenter: parent.horizontalCenter
                height: Theme.itemSizeLarge

                IconButton {
                    icon.source: "image://theme/icon-m-previous"
                    onClicked: {
                        podqast.fast_backward()
                    }
                }
                IconButton {
                    icon.source: "image://theme/icon-l-" + (playing ? "pause" : "play")
                    onClicked: {
                        playing = !playing;
                        if (playing) {
                            playerHandler.play()
                        } else {
                            playerHandler.pause()
                        }
                    }

                }
                IconButton {
                    icon.source: "image://theme/icon-m-next"

                    onClicked: {
                        podqast.fast_forward()
                    }
                }
            }
        }
    }
}
