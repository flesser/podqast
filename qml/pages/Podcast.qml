import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "../components"

Page {
    id: page
    property var url
    property var title
    property var titlefull
    property var description
    property var website
    property var logo_url
    property var altdata

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent
        VerticalScrollDecorator { }
        contentHeight: page.height
        Column {
            id: titlecolumn
            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                id: pageheader
                title: titlefull
            }
        }

        AppMenu { thispage: "Discover" }
        PrefAboutMenu {}

        Component.onCompleted: {
            feedparserhandler.getPodcast(url)
        }

        Connections {
            target: feedparserhandler

            onFeedInfo: {
                console.log(pcdata.title)
                console.log(pcdata.logo_url)
                console.log(pcdata.description)
                console.log(pcdata.descriptionfull)
                podimage.source=pcdata.logo_url
                poddescription.text=pcdata.description
                description=pcdata.descriptionfull
                titlefull=pcdata.title
                console.log(description)
                subscribebutton.enabled=true
                feedparserhandler.getAlternatives(url)
            }

            onFirstEvent: {
                console.log("fevent");
                console.log(pcdata.title)
                fpostlabel.visible = true
                fentryListModel.clear();
                fentryListModel.append(pcdata)
                feedparserhandler.getLastEvent(url)
            }

            onLastEvent: {
                console.log("levent");
                console.log(pcdata.title)
                lpostlabel.visible = true
                lentryListModel.clear();
                lentryListModel.append(pcdata)
            }

            onAlternatives: {
                altdata = pcdata
                altcombo.visible=false
                if(pcdata.altfeeds.length === 0) {
                    console.log("no alternative or wrong mime type")
                    console.log(url)
                }
                else if(pcdata.altfeeds.length === 1) {
                    console.log("we have one alternative")
                    console.log(pcdata.altfeeds[0].url)
                    // url = pcdata.altfeeds[0].url
                } else {
                    console.log("we have more than one alternative")
                    altpods.clear()

                    for (var i=0; i < pcdata.altfeeds.length; i++) {
                        console.log(pcdata.altfeeds[i].title)
                        altpods.append(pcdata.altfeeds[i])
                    }
                    altcombo.visible=true
                }
                feedparserhandler.getFirstEvent(url)
            }
            onSubscribed: {
                subscribebutton.subscribed = true
                subscribebutton.subscribing = false
                subscribebutton.enabled = true
            }
        }

        Row {
            id: inforow
            height: parent.width / 2.5
            width: parent.width
            anchors.margins: 10
            anchors.top: titlecolumn.bottom
            MouseArea {
                width: parent.width
                height: parent.height
                onClicked: pageStack.push(Qt.resolvedUrl("Poddescription.qml"), { description: description,
                                          title: titlefull })
                Column {
                    id: imagecolumn
                    // anchors.left: parent.left
                    width: parent.width / 2.5 - 10
                    height: parent.width / 2.5 - 10

                    Image {
                        id: podimage
                        source: logo_url
                        height: parent.width - 20
                        width: parent.width - 20
                        signal failedToLoad

                        onStatusChanged: {
                            if (status == Image.Error) {
                                source="../../images/podcast.png"
                                failedToLoad()
                            }
                        }
                        BusyIndicator {
                            size: BusyIndicatorSize.Large
                            anchors.centerIn: podimage
                            running: podimage.status != Image.Ready
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                pageStack.push(Qt.resolvedUrl("../pages/PodpostList.qml"), { url: url })
                            }
                        }
                    }
                }
                Column {
                    anchors.left: imagecolumn.right
                    anchors.right: parent.right
                    Label {
                        id: poddescription
                        width: parent.width
                        height: parent.height
                        wrapMode: Text.WordWrap
                        textFormat: Text.StyledText
                        text: description
                        font.pixelSize: Theme.fontSizeExtraSmall
                    }
                }
            }
        }
        Row {
            width: parent.width
            anchors.top: inforow.bottom
            id: podinfo

            Button {
                property bool subscribed: false
                property bool subscribing: false
                id: subscribebutton
                width: parent.width / 3.5
                text: subscribed ? qsTr("Configure") : qsTr("Subscribe")
                onClicked: {
                    if (subscribed == false) {
                        do_subscribe()
                    } else {
                        pageStack.push(Qt.resolvedUrl("PodcastSettings.qml"),
                                       { podtitle: title, url: url })
                    }
                }

                enabled: false

                function do_subscribe() {
                    if(altdata.altfeeds.length > 1) {
                        console.log("combo value: " + altcombo.value)
                        for (var i = 0; i < altdata.altfeeds.length; i++) {
                            if (altdata.altfeeds[i].title === altcombo.value) {
                                url = altdata.altfeeds[i].url
                            }
                        }
                    }
                    console.log(url)
                    subscribing=true
                    enabled=false
                    feedparserhandler.subscribePodcast(url)
                }

                BusyIndicator {
                    size: BusyIndicatorSize.Medium
                    anchors.centerIn: subscribebutton
                    running: subscribebutton.subscribing
                }
            }
            ComboBox {
                id: altcombo
                visible: false
                menu: ContextMenu {
                    MenuItem { text: qsTr("Feed alternatives") }
                    Repeater {
                        model: ListModel { id: altpods }
                        delegate: MenuItem {
                                    text: title
                                    font.pixelSize: Theme.fontSizeSmall
                        }
                    }
                }
            }
        }
        Row {
            id: lpostlabel
            height: Theme.itemSizeExtraSmall
            anchors.top: podinfo.bottom
            visible: false
            Label {
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: Theme.fontSizeSmall
                color: Theme.highlightColor
                text: qsTr("Latest Post")
            }
        }

        SilicaListView {
            id: fpodentries
            anchors.top: lpostlabel.bottom
            width: parent.width
            height: Theme.itemSizeExtraLarge * 1.1

            model: ListModel {
                id: fentryListModel
            }
            delegate: PodcastEntryItem {}
        }
        Row {
            id: fpostlabel
            height: Theme.itemSizeExtraSmall
            anchors.top: fpodentries.bottom
            visible: false
            Label {
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: Theme.fontSizeSmall
                color: Theme.highlightColor
                text: qsTr("First Post")
            }
        }
        SilicaListView {
            id: lpodentries
            anchors.top: fpostlabel.bottom
            width: parent.width
            height: Theme.itemSizeExtraLarge * 1.1

            model: ListModel {
                id: lentryListModel
            }
            delegate: PodcastEntryItem {}
        }
        PlayDockedPanel { }
    }
}
