import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    property var url
    property var podtitle
    property bool subs
    property bool subscribed


    SilicaFlickable {
        anchors.fill: parent
        contentWidth: parent.width

        VerticalScrollDecorator { }

        Column {
            id: downloadConf
            width: parent.width

            DialogHeader {
                title: podtitle
            }
            Label {
                text: qsTr("Posts")
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
            }

            ComboBox {
                enabled: subs || podsubscribe.checked
                id: moveTo
                width: parent.width
                label: qsTr("Move new post to")
                menu: ContextMenu {
                    MenuItem { text: qsTr("Inbox") }
                    MenuItem { text: qsTr("Top of Playlist") }
                    MenuItem { text: qsTr("Bottom of Playlist") }
                    MenuItem { text: qsTr("Archive") }
                }
            }
            Slider {
                id: podAutoLimit
                enabled: subs || podsubscribe.checked
                width: parent.width
                label: qsTr("Automatic post limit")
                minimumValue: 0
                maximumValue: 10
                handleVisible: true
                valueText: value == 0 ? "All" : value
                stepSize: 1
            }
            Slider {
                id: playrate
                enabled: false
                visible: false
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingMedium
                }
                width: parent.width
                label: qsTr("Audio playrate")
                minimumValue: 0.85
                maximumValue: 2.0
                handleVisible: true
                valueText: "1:" + value
                stepSize: 0.15
            }

            Label {
                text: qsTr("Podcast")
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.highlightColor
                visible: ! subs
            }

            TextSwitch {
                id: podsubscribe
                text: qsTr("Subscribe")
                checked: subs
                visible: ! subs
            }
        }

    }
    onOpened: {
        console.log("Title: "  + podtitle)
        feedparserhandler.getPodcastParams(url)
    }

    onAccepted: {
        var data = {
            move: moveTo.currentIndex,
            autolimit: podAutoLimit.value,
            playrate: playrate.value
        }
        if (podsubscribe.checked && ! subscribed) {
            console.log("Subscribing" + url)
            feedparserhandler.subscribePodcastFg(url)
        }
        feedparserhandler.setPodcastParams(url, data)
    }

    Connections {
        target: feedparserhandler
        onPodcastParams: {
            console.log("podcast moveto: " + pcdata.move)
            if (pcdata.move !== -1) {
                moveTo.currentIndex = pcdata.move
            } else {
                moveTo.currentIndex = moveToConf.value
            }
            subs = pcdata.subscribed
            subscribed = subs
            if(pcdata.autolimit) {
                podAutoLimit.value = pcdata.autolimit
            } else {
                podAutoLimit.value = autoLimitConf.value
            }

            if(pcdata.playrate !== 0) {
                playrate.value = pcdata.playrate
            } else {
                playrate.value = globalPlayrateConf.value
            }

            console.log("moveTo:" + moveTo.currentIndex)
        }
    }
}
