import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    Component.onCompleted: {
        queuehandler.getQueueEntries()
        if (doDownloadConf.value && (wifiConnected || doMobileDownConf.value)) {
            queuehandler.downloadAudioAll()
        }
    }

    Connections {
        target: queuehandler
        ignoreUnknownSignals: true
        onCreateList: {
            queuePostModel.clear()
            for (var i = 0; i < data.length; i++) {
                queuePostModel.append(data[i]);
                if (i == 0) {
                    playicon = data[i].logo_url
                }
            }
            if (i > 0) {
                    playeropen = true
            } else {
                    playeropen = false
            }
        }
    }

    Connections {
        target: mediaplayer

        ignoreUnknownSignals: true
        onStopped: {
            console.log("onStopped")
            console.log("status: " + mediaplayer.status)
            if (mediaplayer.status === 7) {
                queuehandler.queueTopToArchive()
            }
            console.log("onStopped - play()")
       }
    }

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        AppMenu { thispage: "Queue" }
        PrefAboutMenu {}

        // Tell SilicaFlickable the height of its content.
        contentHeight: page.height

        Column {
            id: queuetitle

            width: page.width

            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Playlist")
            }
        }
        SilicaListView {
            id: queuepostlist
            anchors.top: queuetitle.bottom
            width: parent.width
            height: page.height - pdp.height - queuetitle.height

            model: ListModel {
                id: queuePostModel
            }
            delegate: QueuePostListItem { }
            ViewPlaceholder {
                enabled: queuePostModel.count == 0
                text: qsTr("No posts")
                hintText: qsTr("Pull down to Discover new podcasts or get posts from Inbox or Library")
                verticalOffset: - queuetitle.height
            }

            // VerticalScrollDecorator {}
        }

        PlayDockedPanel { id: pdp }
    }
}
