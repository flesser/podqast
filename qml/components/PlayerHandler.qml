import QtQuick 2.0
import io.thp.pyotherside 1.4

Python {
    id: playerHandler

    signal playing(string audio_url, int position)
    signal pausing()
    signal stopping()
    signal downloading(int percent)

    Component.onCompleted: {
        setHandler('playing', playing)
        setHandler('pausing', pausing)
        setHandler('downloading', downloading)
        setHandler('stopping', stopping)

        addImportPath(Qt.resolvedUrl('components'));
        importModule('QueueHandler', function () {
            console.log('QueueHandler is now imported')
        })
    }
    onPlaying: {
        console.log("Audio_url: " + audio_url)
        console.log("Seekable: " + mediaplayer.seekable)
        if (audio_url != "") {
            podqast.playrate = globalPlayrateConf.value
            mediaplayer.source = audio_url
            mediaplayer.seek(position - 15 * 1000)
            mediaplayer.play()
            mediaplayer.seek(position)
            podqast.seekPos = position
        } else {
            mediaplayer.play()
        }

        mediaplayer.playbackRate = playrate

        console.log("Duration: ", mediaplayer.duration)
        console.log("Position: ", mediaplayer.position)
        podqast.playing=true
        queuehandler.queueHrTime()
    }
    onPausing: {
        mediaplayer.pause()
        podqast.playing=false
        queuehandler.queueHrTime()
    }
    onStopping: {
        // mediaplayer.stop()
        mediaplayer.pause()
        podqast.playing=false
        queuehandler.queueHrTime()
    }

    onDownloading: {

    }

    function play() {
        call("QueueHandler.queuehandler.queueplay", function() {});
        // call("QueueHandler.queue_play", function() {});
    }

    function pause() {
        call("QueueHandler.queuehandler.queuepause", [mediaplayer.position], function() {});
        // call("QueueHandler.queue_pause", [mediaplayer.position], function() {});
    }

    function stop() {
        console.log("mediaplayer.position" + mediaplayer.position)
        call("QueueHandler.queuehandler.queuestop", [mediaplayer.position], function() {});
        // call("QueueHandler.queue_stop", [mediaplayer.position], function() {});
    }

    function seek(position) {
        call("QueueHandler.queuehandler.queueseek", [position], function() {});
        // call("QueueHandler.queue_seek", [position], function() {});
    }

    function setDuration() {
        console.log("mediaplayer.duration" + mediaplayer.duration / 1000)
        call("QueueHandler.queuehandler.setduration", [mediaplayer.duration / 1000], function() {})
        // call("QueueHandler.queuehandler.set_duration", [mediaplayer.duration / 1000], function() {})
    }
}
