import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {
    // width: ListView.view.width
    height: Theme.itemSizeMedium
    onClicked: pageStack.push(Qt.resolvedUrl("../pages/Podsearch.qml"), { tagname: tagname })

    Label {
        anchors.left: parent.left
        anchors.leftMargin: Theme.paddingMedium
        anchors.verticalCenter: parent.verticalCenter
        text: tagname
    }
    IconButton {
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        icon.source: "image://theme/icon-m-right"
        onClicked: pageStack.push(Qt.resolvedUrl("../pages/Podsearch.qml"), { tagname: tagname })
    }
}
