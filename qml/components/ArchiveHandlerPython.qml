import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4

Python {
    id: archivehandler

    signal createArchiveList(var data)
    signal archivePodList(var data)

    Component.onCompleted: {
        setHandler("createArchiveList", createArchiveList)
        setHandler("archivePodList", archivePodList)

        addImportPath(Qt.resolvedUrl('.'));
        importModule('ArchiveHandler', function () {
            console.log('ArchiveHandler is now imported')
        })
    }

    function getArchiveEntries(podurl) {
        if (podurl === "home") {
            call("ArchiveHandler.archivehandler.getarchiveposts", function() {});
            // call("ArchiveHandler.get_archive_posts", function() {});
        } else {
            call("ArchiveHandler.archivehandler.getarchiveposts", [podurl], function() {});
            // call("ArchiveHandler.get_archive_posts", [podurl], function() {});
        }
    }

    function getArchivePodData() {
        call("ArchiveHandler.archivehandler.getarchivepoddata", function() {});
        // call("ArchiveHandler.get_archive_pod_data", function() {});
    }
}
