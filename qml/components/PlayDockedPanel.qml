import QtQuick 2.0
import Sailfish.Silica 1.0

DockedPanel {
    id: playdockedpanel

    width: parent.width
    height: Theme.itemSizeSmall
    open: playeropen

    dock: Dock.Bottom

    Rectangle {
        anchors.fill: parent
        color: Theme.highlightDimmerColor
        opacity: 0.7
    }

    Row {
        id: row
        anchors.centerIn: parent
        IconButton {
            icon.source: "image://theme/icon-m-previous"
            onClicked: {
                podqast.fast_backward()
            }
        }
        IconButton {
            icon.source: "image://theme/icon-m-" + (playing ? "pause" : "play")
            onClicked: {
                playing = !playing;
                if (playing) {
                    playerHandler.play()
                } else {
                    playerHandler.pause()
                }
            }
        }
        IconButton {
            icon.source: "image://theme/icon-m-next"
            onClicked: {
                podqast.fast_forward()
            }
        }
        IconButton {
            id: playpanelicon
            icon.source: playicon == "" ? "../../images/podcast.png" : playicon
            icon.width: 60
            icon.height: 60
            onClicked: {
                queuehandler.getFirstEntry()
            }
            Connections {
                target: queuehandler
                onFirstEntry: {
                    pageStack.push(Qt.resolvedUrl("../pages/PostDescription.qml"),
                                   { title: data.title, detail: data.detail,
                                       length: data.length, date: data.fdate, duration: data.duration,
                                       href: data.link
                                   })
                }
            }
        }
        IconButton {
            id: podimage
            icon.source: "image://theme/icon-m-right"
            onClicked: pageStack.push(Qt.resolvedUrl("../pages/Player.qml"))
        }
    }
}
